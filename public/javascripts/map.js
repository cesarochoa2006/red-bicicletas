var mymap = L.map('map').setView([5.779438 ,-73.117921], 13);

L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
}).addTo(mymap);

var marker = L.marker([5.779438 ,-73.117921]).addTo(mymap);
var marker = L.marker([5.781234 ,-73.118351]).addTo(mymap);
var marker = L.marker([5.761978 ,-73.117351]).addTo(mymap);
