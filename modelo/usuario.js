var mongoose = require('mongoose');
var Reserva = require('./reserva');
var Schema = mongoose.Schema;
const bcrypt = require('bcrypt');
const crypto = require('crypto');
var  uniqueValidator = require('mongoose-unique-validator');
const mailer = require('../mailer/mailer');
const Token = require('../modelo/token');
const saltRounds = 10;


const validateEmail = function(email) {
    const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email);
}

var usuarioSchema = new Schema ({
    nombre: {
        type: String,
        trim: true,
        required: [true, 'El nombre es obligatorio']
    },
    email: {
        type: String,
        trim: true,
        required: [true, 'El email es obligatorio'],
        unique: true,
        lowercase: true,
        validate: [validateEmail, 'Por favor, ingrese un email válido'],
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/]
    },
    facebookId: String,
    googleId: String,
    password: {
        type: String,
        required: [true, 'El password es obligatorio']
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado: {
        type: Boolean,
        default: false
    }
});

usuarioSchema.plugin(uniqueValidator, {message: 'El {PATH} ya se encuentra asociado a otro usuario'});

usuarioSchema.pre('save', function(next){
    if(this.isModified('password')) {
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next();
});

usuarioSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
}

usuarioSchema.methods.reservar = function(biciId, desde, hasta, cb){
    var reserva = new Reserva({usuario: this._id, bicicleta: biciId, desde, hasta});
    console.log(reserva);
    reserva.save(cb);
}

usuarioSchema.methods.enviar_email_bienvenida = function(cb) {
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    console.log('Nuevo Token: ' + token);
    const email_destination = this.email;
    token.save(function(err){
        if(err) {return console.log(err.message);}
        const mailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'Verificacion de cuenta',
            text: 'Hola,\n\n' + 'Por favor, para verificar su cuenta haga click en este link:\n' + 'http://localhost:3000' + '\/token/confirmation\/' + token.token + '.\n'
        };
        mailer.sendMail(mailOptions, function(err){
            if(err) {return console.log(err.message);}
            console.log('Se ha enviado un email de verificacion a la siguiente cuenta: ' + email_destination + '.');
        });
    });
}

usuarioSchema.statics.findOneOrCreateByGoogle = function findOneOrCreate(condition, callback){
    const self = this;
    console.log(condition);
    self.findOne({
        $or:[ {'googleId': condition.id}, {'email': condition.emails[0].value} ]}, function(err, result){
            console.log('RESULTADO: ' + result);
            if(result) return callback(err, result)
            
            console.log('*******CONDITION*******');
            console.log(condition);

            var values = {};
            values.googleId = condition.id;
            values.email = condition.emails[0].value;
            values.nombre = condition.displayName || 'SIN NOMBRE';
            values.verificado = true;
            values.password = crypto.randomBytes(16).toString('hex');
            //console.log('CONTRASEÑA: ' + values.password);

            console.log('*******VALUES*******');
            console.log(values);

            self.create(values, function(err, result){
                if(err) console.log(err);
                return callback(err, result);
            }); 
        }
    )
}

usuarioSchema.statics.findOneOrCreateByFacebook = function(condition, callback){
    const self = this;
    console.log(condition);
    self.findOne({
        $or:[
            {'facebookId': condition.id}, {'email': condition.emails[0].value}
        ]
    }, (err, result) => {
        if(result) {
            callback(err, result)
        } else {
            console.log('*******CONDITION*******');
            console.log(condition);

            var values = {};
            values.facebookId = condition.id;
            values.email = condition.emails[0].value;
            values.nombre = condition.displayName || 'SIN NOMBRE';
            values.verificado = true;
            values.password = crypto.randomBytes(16).toString('hex');
            //console.log('CONTRASEÑA: ' + values.password);

            console.log('*******VALUES*******');
            console.log(values);
            self.create(values, (err, result) => {
                if(err) {console.log(err);}
                return callback(err, result)
            })
        }
    })
};

module.exports = mongoose.model('Usuario', usuarioSchema);