var mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
var Schema = mongoose.Schema;

/**
 * Modelo de bicicleta 
 */
var bicicletaSchema = new Schema ({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number], index:{type: '2dsphere', sparse: true}
    }
});

bicicletaSchema.statics.crearInstancia = function(code, color, modelo, ubicacion){
    return new this ({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
};

bicicletaSchema.methods.toString = function(){
    return 'code: ' + this.code + ' | color: ' + this.color;
};

/** 
 * Representación de todas las bicicletas
 */
bicicletaSchema.statics.todas = function(cb){
    return this.find({}, cb);
};


/**
 * Funciones bicicleta
 */
bicicletaSchema.statics.agregar = function(bici, cb){
    this.create(bici, cb);
};

bicicletaSchema.statics.editar = function(bici, cb){
    this.updateOne({
        code: bici.code
    },{
        modelo : bici.modelo,
        color: bici.color,
        ubicacion: bici.ubicacion
    },
    cb);
};

bicicletaSchema.statics.encontrarPorCodigo = function(codigo, cb){
    return this.findOne({code: codigo}, cb);
};

bicicletaSchema.statics.eliminarPorCodigo = function(codigo, cb){
    return this.deleteOne({code: codigo}, cb);
};

module.exports = mongoose.model('Bicicleta', bicicletaSchema);