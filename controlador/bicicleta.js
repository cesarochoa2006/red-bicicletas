var Bicicleta = require('../modelo/bicicleta');

exports.lista_bicicletas = (req, res) => {
    Bicicleta.find({}, function(err, bicicletas) {
        if(err) return res.status(500).send(err);
        res.render('bicicletas/index', { bicis: bicicletas});
    });
    /* res.render('bicicletas/index', { bicis: Bicicleta.todas }); */
};

exports.bicicletas_agregar_get = (req,res) => {
    res.render('bicicletas/agregar');
}

exports.bicicletas_agregar_post = (req,res) => {
    const cuerpo = req.body;
    var bici = new Bicicleta(cuerpo.id, cuerpo.color, cuerpo.modelo,
         [cuerpo.latitud,cuerpo.longitud]);
    Bicicleta.agregar(bici);
    res.redirect('/bicicletas')
}

exports.bicicletas_eliminar_post = (req,res) => {
    Bicicleta.eliminarPorId(req.body.id);
    res.redirect('/bicicletas')
}


exports.bicicletas_editar_get = (req,res) => {
    const id = req.params.id;
    console.log(id);
    const bici = Bicicleta.todas[Bicicleta.encontrar(id)]
    res.render('bicicletas/editar', { bici: bici });
}

exports.bicicletas_editar_post = (req,res) => {
    const cuerpo = req.body;
    var bici = new Bicicleta(cuerpo.id, cuerpo.color, cuerpo.modelo,
         [cuerpo.latitud,cuerpo.longitud]);
    Bicicleta.editar(bici);
    res.redirect('/bicicletas')
}
