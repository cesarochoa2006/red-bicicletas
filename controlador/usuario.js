var Usuario = require('../modelo/usuario');

module.exports = {

    list: function(req, res, next){
        Usuario.find({}, function(err, usuarios) {
            if(err) return res.status(500).send(err);
            res.render('usuarios/index', { usuarios: usuarios});
        });
    },

    update_get: function(req, res, next){
        console.log('id: ',req.params);
        Usuario.findById(req.params.id, function(err, usuario, next){
            if(err){
                console.log('error: ',err);
                return res.status(500).send(err);

            } 
            console.log('usuario: ',usuario);
            res.render('usuarios/update', { errors: {}, usuario: usuario});
        });
    },

    update_post: function(req, res, next){
        var update_values = {nombre: req.body.nombre};
        Usuario.findByIdAndUpdate(req.params.id, update_values, function(err, usuario){
            if(err) return res.render('usuarios/upadate', {errors: err.errors, usuario: new Usuario()});
            return res.redirect('/usuarios');
        });
    },

    create_get: function(req, res, next){
        res.render('usuarios/create', { errors: {}, usuario: new Usuario()});
    },

    create_post: function(req, res, next){
        
        var nuevo = { nombre: req.body.nombre, email: req.body.email,  password: req.body.password, confirm_password: req.body.confirm_password };
        console.log('cuerpo crear: ',nuevo);
        Usuario.create(nuevo, function(err, usuario){
            //if(err) return res.status(500).send(err);
            if(err){
                console.log("error", err);
                return res.render('usuarios/create', { errors: err.errors, usuario: new Usuario() });
            }    

            usuario.enviar_email_bienvenida();
            return res.redirect('/usuarios');
        });
        
    },

    delete_post: function(req, res, next){
        Usuario.findByIdAndDelete(req.params.id, function(err){
            if(err) return next(err);
            return res.redirect('/usuarios');
        });
    }

}

