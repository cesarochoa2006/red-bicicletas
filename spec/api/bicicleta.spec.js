var Bicicleta = require('../../modelo/bicicleta');
var request = require('request');
var mongoose = require('mongoose');
const url = 'http://localhost:3000/api/v1/bicicletas/';

describe('Bicicleta API', () => {
    beforeEach(function (done) {
        var mongoDB = 'mongodb://localhost/red_bicicletas';
        mongoose.connect(mongoDB, { useNewUrlParser: true });
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'Error al conectar'));
        db.once('open', () => {
            console.log('Conectado!');
            Bicicleta.deleteMany({}, function (err, success) {
                if (err) console.log(err);
                done();
            });
        });
    });
    afterEach(function (done) {
        Bicicleta.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            done();
            if(success){
                console.log('Elementos borrados');
            }
        });
    });
    describe('GET BICICLETAS /', () => {
        it('Status 200', ((done) => {
            request.get(url, (error, response, body) => {
                expect(response.statusCode).toBe(200);
                const cuerpo = JSON.parse(body);
                expect(typeof cuerpo.respuesta).toEqual('object');                
                expect(cuerpo.respuesta.length).toBe(0);
                done();
            });
        })
        );
    });

    describe('GET BICICLETA POR ID/', () => {
        it('Status 200', ((done) => {
            Bicicleta.deleteMany({}, function (err, success) {
                if (err) console.log(err);               
                 request.get(`${url}1000`, function (error, response, body) {
                     expect(response.statusCode).toBe(200);
                     expect(JSON.parse(body).respuesta).toBeNull;
                     done();
                 });
            });
        })
        );
    });

    describe('POST BICICLETA ', () => {
        it('Status 200', (done) => {
            var headers = { 'content-type': 'application/json' };
            var bicis ="{\r\n    \"bicicleta\": {\r\n        \"id\": 20,\r\n        \"color\": \"Morado\",\r\n        \"modelo\": \"urbana\",\r\n        \"ubicacion\": [\r\n            5.789438,\r\n            -73.127921\r\n        ]\r\n    }\r\n}";
            request.post({
                headers: headers,
                url: url,
                body: bicis
            }, function (error, response, body) {
                expect(response.statusCode).toBe(200);
                expect(JSON.parse(body).respuesta).toBe('La bicicleta fue agregada exitosamente');                
                done();
            });
        });
    });

    describe('DELETE BICICLETA /DELETE', () => {
        it('Status 200', (done) => {
            request.delete(url + '3000', (error, response, body) => {
                expect(response.statusCode).toBe(200);
                expect(JSON.parse(body).respuesta).toBe('La bicicleta con codigo 3000 no existe');
                done();
            });
        });
    });

    describe('PUT BICICLETA /PUT', () => {
        it('Status 200', (done) => {
            const id = 1;
            var bici = Bicicleta.crearInstancia(id, 'Negro', 'Urbana', [5.743343354, -73.11222353]);
            Bicicleta.agregar(bici, (error) => {
            expect(error).toBeNull();
            var headers = { 'content-type': 'application/json' };
            var aBici = '{ "bicicleta": { "id": 1, "color": "Morado", "modelo": "Urbana", "ubicacion": [ 5.789438, -73.127921 ] } }';
            request.put({
                headers: headers,
                url: url,
                body: aBici
            }, function (error, response, body) {
                expect(response.statusCode).toBe(200);
                expect(JSON.parse(body).respuesta).toBe('La bicicleta fue actualizada exitosamente');
                done();
            });
        });
        });
    }); 

});