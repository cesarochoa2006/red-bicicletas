var express = require('express');
var router = express.Router();
var controladorBicicletas = require('../controlador/bicicletaApiControlador');

router.get('/', controladorBicicletas.lista_bicicletas);
router.get('/:id', controladorBicicletas.bicicletaPorId);
router.post('/', controladorBicicletas.agregar_bicicleta);
router.put('/', controladorBicicletas.editar_bicicleta);
router.delete('/:id', controladorBicicletas.eliminar_bicicleta);

module.exports = router;