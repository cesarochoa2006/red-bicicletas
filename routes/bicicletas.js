var express = require('express');
var router = express.Router();
var controladorBicicletas = require('../controlador/bicicleta');

router.get('/',controladorBicicletas.lista_bicicletas);
router.get('/agregar',controladorBicicletas.bicicletas_agregar_get);
router.post('/agregar',controladorBicicletas.bicicletas_agregar_post);
router.post('/:id/eliminar',controladorBicicletas.bicicletas_eliminar_post);
router.get('/:id/editar',controladorBicicletas.bicicletas_editar_get);
router.post('/:id/editar',controladorBicicletas.bicicletas_editar_post);
module.exports = router;