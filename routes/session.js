var express = require('express');
var router = express.Router();
var passport = require('../config/passport');

/**
 * Login 
 */
router.get('/login', function (req, res, next) {
    if(req.user){
        res.redirect('/');
    }
    res.render('session/login', { title: 'Login' });
});


router.post('/login', function (req, res, next) {
    passport.authenticate('local', function (err, user, info) {
        if (err) return next(err);
        if (!user) return res.render('session/login', { info });
        req.logIn(user, function (err) {
            if (err) return next(err);
            return res.redirect('/');
        })
    })(req, res, next)
});

router.get('/logout', function (req, res, next) {
    req.logout();
    res.redirect('/');
});
/**
 * Recuperacion de cuenta 
 */
router.get('/forgotPassword', function (req, res, next) {
    res.render('session/forgotPassword', { title: 'Recuperar usuario Paso 1' });
});

router.post('/forgotPassword', function (req, res, next) {
    res.render('session/forgotPasswordMessage', { title: 'Recuperar usuario Paso 2' });
});


module.exports = router;